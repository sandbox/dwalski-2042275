<?php

public function commerce_jambopay_rurl(){
  $settings = commerce_payment_method_instance_load('commerce_jambopay|commerce_payment_commerce_jambopay');
  
  $sharedkey = $settings['settings']['shared_key'];

  if (isset($_POST['JP_PASSWORD'])) {

    $JP_TRANID = $_POST['JP_TRANID'];
    $JP_MERCHANT_ORDERID = $_POST['JP_MERCHANT_ORDERID'];
    $JP_ITEM_NAME = $_POST['JP_ITEM_NAME'];
    $JP_AMOUNT = $_POST['JP_AMOUNT'];
    $JP_CURRENCY = $_POST['JP_CURRENCY'];
    $JP_TIMESTAMP = $_POST['JP_TIMESTAMP'];
    $JP_PASSWORD = $_POST['JP_PASSWORD'];
    $JP_CHANNEL = $_POST['JP_CHANNEL'];

    $str = $JP_MERCHANT_ORDERID . $JP_AMOUNT . $JP_CURRENCY . $sharedkey . $JP_TIMESTAMP;

    $order = commerce_order_load($JP_MERCHANT_ORDERID);


  //**************** VERIFY *************************
    if (md5(utf8_encode($str)) == $JP_PASSWORD) {
      //VALID TRANSACTION. Complete the order.
      $transaction = commerce_payment_transaction_new('commerce_jambopay', $JP_MERCHANT_ORDERID);
      $transaction->instance_id = $settings['instance_id'];
      $transaction->currency_code = $JP_CURRENCY;
      $transaction->amount = $JP_AMOUNT;
      $transaction->remote_id = $JP_TRANID;
      $transaction->status = COMMERCE_PAYMENT_STATUS_SUCCESS;
      $transaction->message = 'Payment on Jambopay has been successful.';
      
      commerce_payment_transaction_save($transaction);
      
      commerce_payment_redirect_pane_next_page($order);

      $message = 'Transaction completed successfully';

      return $message;
    } 
    else{
      //INVALID TRANSACTION..
      $transaction = commerce_payment_transaction_new('commerce_jambopay', $JP_MERCHANT_ORDERID);
      $transaction->instance_id = $settings['instance_id'];
      $transaction->currency_code = $JP_CURRENCY;
      $transaction->amount = $JP_AMOUNT;
      $transaction->remote_id = $JP_TRANID;
      $transaction->status = COMMERCE_PAYMENT_STATUS_FAILURE;
      $transaction->message = 'Payment on Jambopay has been unsuccessful.';
      
      commerce_payment_transaction_save($transaction);
      
      commerce_payment_redirect_pane_next_page($order);

      $message = 'Transaction could not be completed successfully';

      return $message;
    }
  }
}